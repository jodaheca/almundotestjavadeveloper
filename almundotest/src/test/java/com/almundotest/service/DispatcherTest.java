package com.almundotest.service;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * Test con los cuales se evalua la concurrencia del CalLCenter
 */
public class DispatcherTest {

    /**
     * Solo deben aparecer empleados de tipo CAJERO en la consola
     * @throws InterruptedException
     */
    @Test
    public void dispatchCall7Thread() throws InterruptedException {
        Dispatcher callCenter = new Dispatcher(7);

        for (int i = 1; i <= 15; i++) {
            callCenter.dispatchCall(Integer.toString(i));
        }
        TimeUnit.SECONDS.sleep(30);
    }

    /**
     * Solo deben aparecer empleados de tipo CAJERO  y SUPERVISOR en la consola
     * @throws InterruptedException
     */
    @Test
    public void dispatchCall9Thread() throws InterruptedException {
        Dispatcher callCenter = new Dispatcher(9);

        for (int i = 1; i <= 15; i++) {
            callCenter.dispatchCall(Integer.toString(i));
        }
        TimeUnit.SECONDS.sleep(30);
    }

    /**
     * Aparecen empleados de tipo CAJERO, SUPERVISOR y GERENTE en la consola
     * @throws InterruptedException
     */
    @Test
    public void dispatchCall10Thread() throws InterruptedException {
        Dispatcher callCenter = new Dispatcher(10);

        for (int i = 1; i <= 20; i++) {
            callCenter.dispatchCall(Integer.toString(i));
        }
        TimeUnit.SECONDS.sleep(30);
    }
}