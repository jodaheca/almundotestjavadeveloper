package com.almundotest.service;

import com.almundotest.model.Empleado;
import com.almundotest.model.TipoEmpleado;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Supplier;

/**
 * clase en la cual se procesan las llamadas que entran al CallCenter
 */
public class Dispatcher {

    private ExecutorService callCenter;
    private List<Empleado> cajerosDisponibles;
    private List<Empleado> supervisoresDisponibles;
    private List<Empleado> gerentesDisponibles;

    public Dispatcher (Integer cantidadHilosEjeutar) {
        /**
         * Se inicializa el call center indicando la cantidad de hilos a crear
         * Se setean los empleados disponibles
         */
        callCenter = Executors.newFixedThreadPool(cantidadHilosEjeutar);
        cajerosDisponibles = getCajerosDisponibles();
        supervisoresDisponibles = getSupervisoresDisponibles();
        gerentesDisponibles = getGerentesDisponibles();;
    }

    private List<Empleado> getCajerosDisponibles() {
        List<Empleado> empleados = new ArrayList();
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.OPERADOR).nombre("CARLOS").disponible(true).build());
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.OPERADOR).nombre("ANDRES").disponible(true).build());
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.OPERADOR).nombre("ADAN").disponible(true).build());
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.OPERADOR).nombre("DANIEL").disponible(true).build());
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.OPERADOR).nombre("ANDREA").disponible(true).build());
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.OPERADOR).nombre("CAMILA").disponible(true).build());
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.OPERADOR).nombre("ADRIANA").disponible(true).build());
        return empleados;
    }

    private List<Empleado> getSupervisoresDisponibles() {
        List<Empleado> empleados = new ArrayList();
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.SUPERVISOR).nombre("JUAN").disponible(true).build());
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.SUPERVISOR).nombre("LUIS").disponible(true).build());
        return empleados;
    }

    private List<Empleado> getGerentesDisponibles() {
        List<Empleado> empleados = new ArrayList();
        empleados.add(Empleado.builder().tipoEmpleado(TipoEmpleado.DIRECTOR).nombre("KELY").disponible(true).build());
        return empleados;
    }


    /**
     * procesa las llamadas recibidas
     */
    public void dispatchCall(String nombreCliente) throws InterruptedException {
        Supplier<Empleado> customerCall = () -> {
            Empleado empleado = getEmpleado();
            if (empleado != null){
                int duracionLlamada = ThreadLocalRandom.current().nextInt(5, 10);
                System.out.println("Empleado " + empleado.getTipoEmpleado()+ " " + empleado.getNombre() +
                        " atiende cliente " + nombreCliente + " llamada de " +duracionLlamada + " segundos");
                try {
                    TimeUnit.SECONDS.sleep(duracionLlamada);
                } catch (Exception e) {
                    System.out.println("Llamada caida");
                }
            }
            empleado.setDisponible(true);
            return empleado;
        };

        System.out.println("Cliente " + nombreCliente + " esta esperando");
        CompletableFuture.supplyAsync(customerCall, callCenter)
                .thenAccept(
                        empleadoLibre -> {
                            if (empleadoLibre != null){
                                System.out.println(String.format(String.format("llamada finalizada %s ", empleadoLibre)));
                            }
                        });
    }

    /**
     * Método que verifica que empleados está disponibles y retorna el primero que encuentre teniendo en
     * cuenta el Tipo de Empleado
     * @return
     */
    private Empleado getEmpleado() {
        synchronized(this){
            Empleado empleado = cajerosDisponibles.stream().filter(
                    Empleado::isDisponible).findFirst().orElse(null);
            if (empleado == null) {
                empleado = supervisoresDisponibles.stream().filter(
                        Empleado::isDisponible).findFirst().orElse(null);
                if (empleado == null) {
                    empleado = gerentesDisponibles.stream().filter(
                            Empleado::isDisponible).findFirst().orElse(null);
                    if (empleado == null){
                        return null;
                    }
                }
            }
            empleado.setDisponible(false);
            return empleado;
        }
    }
}
