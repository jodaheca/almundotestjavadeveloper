package com.almundotest.model;

public enum TipoEmpleado {

    OPERADOR(TipoEmpleado.OPERADOR_CODE, "Empleado Operador"),
    SUPERVISOR(TipoEmpleado.SUPERVISOR_CODE, "Empleado Supervisor"),
    DIRECTOR(TipoEmpleado.DIRECTOR_CODE, "Empleado Director");

    private final String id;
    private final String descripcion;

    private static final String OPERADOR_CODE = "OP";
    private static final String SUPERVISOR_CODE = "SP";
    private static final String DIRECTOR_CODE = "DR";

    TipoEmpleado(String id, String descripcion){
        this.id = id;
        this.descripcion = descripcion;
    }
}
