package com.almundotest.model;

import lombok.*;

/**
 * Información del empleado
 */
@Builder
@Data
public class Empleado {

    String nombre;
    TipoEmpleado tipoEmpleado;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    boolean disponible;

    public boolean isDisponible(){
        synchronized (this) {
            return disponible;
        }
    }

    public void setDisponible(boolean disponible){
        synchronized (this){
            this.disponible = disponible;
        }
    }
}
