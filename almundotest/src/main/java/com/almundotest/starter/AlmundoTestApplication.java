package com.almundotest.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlmundoTestApplication {

	public static void main(String[] args){
		SpringApplication.run(AlmundoTestApplication.class, args);
	}

}
